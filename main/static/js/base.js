const openMenuLeft = document.querySelector('#show-menu-left')
const hideMenuIconLeft = document.querySelector('#hide-menu-left')
const leftMenu = document.querySelector('#nav-menu-left')

const openMenuRight = document.querySelector('#show-menu-right')
const hideMenuIconRight = document.querySelector('#hide-menu-right')
const rightMenu = document.querySelector('#nav-menu-right')

const hideMenu = document.querySelector('#hide-menu')

openMenuLeft.addEventListener('click', function() {
    leftMenu.classList.add('active')
    rightMenu.classList.remove('active')
})

hideMenuIconLeft.addEventListener('click', function() {
    leftMenu.classList.remove('active')
})

openMenuRight.addEventListener('click', function() {
    rightMenu.classList.add('active')
    leftMenu.classList.remove('active')
})

hideMenuIconRight.addEventListener('click', function() {
    rightMenu.classList.remove('active')
})

hideMenu.addEventListener('click', function() {
    leftMenu.classList.remove('active')
    rightMenu.classList.remove('active')
})