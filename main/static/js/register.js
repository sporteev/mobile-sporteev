const passwordValidation = () => {
    let password1 = document.getElementById("password")
    let password2 = document.getElementById("ulangi-password")

    if (password1.value != password2.value) {
        password2.style.outline = "2px solid red"
    } else if (password1.value == password2.value) {
        password2.style.outline = 0
    } 
}

const button_submit = document.getElementById("button-submit")
button_submit.addEventListener('click', function(e) {
    let password1 = document.getElementById("password")
    let password2 = document.getElementById("ulangi-password")

    if (password1.value != password2.value) {
        e.preventDefault()
        Swal.fire(
            'Password not match',
            '',
            'error'
        )
    } 
})