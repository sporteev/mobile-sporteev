let button = document.getElementById('submit')

button.addEventListener('click', async function (e) {
    const username = document.getElementsByName('username')[0].value 
    const password = document.getElementsByName('password')[0].value
    if (username === "" || password === "") {
        Swal.fire(
            {icon:'error', text:"Lengkapi form terlebih dahulu", confirmButtonColor:"#3D4058"}
        )
        return;
    }
    e.preventDefault()
})