from django.contrib import admin
from .models import CustomUser, Pengguna, PhotoTest
from django.contrib.auth.admin import UserAdmin

# Register your models here.
admin.site.register(CustomUser, UserAdmin)
admin.site.register(Pengguna)
admin.site.register(PhotoTest)