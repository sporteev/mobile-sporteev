from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path("", views.login, name="login"),
    path("logout/", views.logout, name="logout"),
    path("register/", views.register, name="register"),
    path("register/pengguna/", views.registrasi_pengguna, name="registrasi-pengguna"),
    path("about/", views.about, name="about"),
    path("faq/", views.faq, name="faq"),
    path("bantuan/", views.bantuan, name="bantuan"),
    path("pengaturan/", views.pengaturan, name="pengaturan"),
]