from django.urls import path

from . import views

app_name = 'profil'

urlpatterns = [
    path("", views.profil, name="profil"),
    path("edit-profil/", views.edit_profil, name="edit-profil"),
]