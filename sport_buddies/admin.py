from django.contrib import admin
from .models import SportCommunity, Buddy
from django.contrib.auth.admin import UserAdmin

# Register your models here.
admin.site.register(SportCommunity)
admin.site.register(Buddy)
