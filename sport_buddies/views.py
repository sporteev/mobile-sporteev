from django.http import request, HttpResponse
from django.shortcuts import redirect, render, resolve_url
from django.views.decorators.csrf import csrf_exempt
from django.utils.datastructures import MultiValueDictKeyError
from .models import Buddy, SportCommunity
from main.models import CustomUser, Pengguna

# Create your views here.

def find_sport_buddies(request):
    pengguna = Pengguna.objects.get(user=request.user)
    list_pengguna = list(Pengguna.objects.all().exclude(user=pengguna.user))
    context = {
        'list_pengguna':list_pengguna 
    }
    return render(request, 'sport_buddies/find-sport-buddies.html', context)

def list_buddies(request):
    pengguna = Pengguna.objects.get(user=request.user)
    list_buddy_tmp = list(Buddy.objects.filter(user_id=pengguna).filter(status=3))
    list_buddy = []
    for buddy in list_buddy_tmp :
        list_buddy.append(buddy.buddy_id)

    context = {
        'list_buddy':list_buddy
    }
    return render(request, 'sport_buddies/list-buddies.html', context)

def user_detail(request,username_buddy):
    user_buddy = CustomUser.objects.get(username=username_buddy)
    buddy = Pengguna.objects.get(user=user_buddy)
    foto = ""
    print(foto)
    try:
        foto = buddy.photo.url
    except:
        foto = False

    context = {
        'foto': foto,
        'nama_depan': buddy.user.first_name,
        'nama_belakang': buddy.user.last_name,
        'username': buddy.user.username,
    }
    return render(request, "sport_buddies/user-detail.html", context)

 
def list_request(request):
    pengguna = Pengguna.objects.get(user=request.user)
    list_request = list(Buddy.objects.filter(user_id=pengguna, status=1)) 
    print(list_request)
    for req in list_request :
        print(req)
    context = {
        'list_request':list_request 
    }
    return render(request, 'sport_buddies/list-request.html', context)


def add_buddies(request,username_buddy):
    pengguna = Pengguna.objects.get(user=request.user)
    user_buddy = CustomUser.objects.get(username=username_buddy)
    calon_buddy = Pengguna.objects.get(user=user_buddy)
    buddy = Buddy.objects.create(
            user_id = pengguna,
            buddy_id = calon_buddy,
            status = 1
        )
    buddy.save()
    print("masuk sini")
    return redirect("/sport-buddies/user-detail/"+username_buddy)


def accept_request(request, username_buddy):
    pengguna = Pengguna.objects.get(user=request.user)
    user_calon_buddy = CustomUser.objects.get(username=username_buddy)
    calon_buddy = Pengguna.objects.get(user=user_calon_buddy)
    buddyUser = Buddy.objects.get(user_id=pengguna, buddy_id=calon_buddy)
    buddyUser.status = 3
    buddyUser.save()
    buddyReverse = Buddy.objects.create(
            user_id = calon_buddy,
            buddy_id = pengguna,
            status = 3
        )
    buddyReverse.save()
    return redirect("/sport-buddies/list-request")


def reject_request(request, username_buddy):
    pengguna = Pengguna.objects.get(user=request.user)
    user_calon_buddy = CustomUser.objects.get(username=username_buddy)
    calon_buddy = Pengguna.objects.get(user=user_calon_buddy)
    buddyUser = Buddy.objects.filter(user_id=pengguna, buddy_id=calon_buddy)
    buddyUser.status = 2
    # buddyUser.delete()
    return redirect("/sport-buddies/list-request")

def leaderboard(request):
    list_pengguna = list(Pengguna.objects.all().order_by('total_exp'))
    
    context = {
        'list_pengguna':list_pengguna 
    }
    return render(request, 'sport_buddies/leaderboard.html', context)

# def update_level(request):
#     pengguna = Pengguna.objects.get(user=request.user)
#     pengguna.level = (pengguna.total_exp // 100) + 1        #naik level tiap 100 exp, mulai dari level 1
#     pengguna.current_exp = pengguna.total_exp % 100
#     pengguna.save()