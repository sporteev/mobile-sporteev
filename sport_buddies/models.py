from pyexpat import model
from telnetlib import STATUS
from typing import Callable
from django.db import models
from django.db.models.deletion import CASCADE
from main.models import Pengguna
from django_dropbox_storage.storage import DropboxStorage
# from location_field.models.plain import PlainLocationField
from django.db.models.fields.related import ManyToManyField

# Create your models here.

class Buddy(models.Model):
    user_id = models.ForeignKey(Pengguna, related_name='%(class)s_requests_user_id',on_delete=CASCADE, blank=False)
    buddy_id = models.ForeignKey(Pengguna, related_name='%(class)s_requests_buddy_id',on_delete=CASCADE, blank=False)
    STATUS = (
        (0, "Draft"),
        (1, "Pending"),
        (2, "Rejected"),
        (3, "Accepted"),
    )
    status = models.IntegerField(choices=STATUS, default=0)
    
class SportCommunity(models.Model):
    nama_community = models.CharField(max_length=50, blank=False)
    RANK = (
        (0, "Jagoan di Hati Orang Tua"),
        (1, "Jagoan RW"),
        (2, "Jagoan Kompleks"),
        (3, "Jagoan Kecamatan"),
        (4, "Jagoan Kota"),
        (5, "Jagoan Provinsi"),
        (6, "Jagoan Nasioanal"),
        (7, "Jagoan Internasioanal"),
    )
    rank = models.IntegerField(choices=RANK, default=0)
    lokasi =  models.CharField(max_length=100, blank=False)
    level = models.IntegerField(default=1)
    exp = models.IntegerField(default=0)
    captain = models.ForeignKey(Pengguna, on_delete=CASCADE, blank=False)

    def __str__(self):
        return self.nama_community

# class RequestBuddy(models.Model):
#     user_id = models.ForeignKey(Pengguna, related_name='%(class)s_requests_user_id',on_delete=CASCADE, blank=False)
#     buddy_id = models.ForeignKey(Pengguna, related_name='%(class)s_requests_buddy_id',on_delete=CASCADE, blank=False)
#     STATUS = (
#         (0, "Draft"),
#         (1, "Pending"),
#         (2, "Rejected"),
#         (3, "Accepted"),
#     )
#     status = models.IntegerField(choices=STATUS, default=0)
#     def __str__(self):
#         return self.buddy_id
