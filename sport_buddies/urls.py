from urllib import request
from django.urls import path

from . import views

app_name = 'sport_buddies'

urlpatterns = [
    path("", views.list_buddies, name="list_buddies"),
    path("list-request/", views.list_request, name="list-request"),
    path("user-detail/<str:username_buddy>", views.user_detail, name="user-detail"),
    path("add-buddies/<str:username_buddy>", views.add_buddies, name="add-buddies"),
    path("accept-request/<str:username_buddy>", views.accept_request, name="accept-request"),
    path("reject-request:/<str:username_buddy>", views.reject_request, name="reject-request"),
    path("find-sport-buddies/", views.find_sport_buddies, name="find-sport-buddies"),
    path("leaderboard/", views.leaderboard, name="leaderboard"),

]
 