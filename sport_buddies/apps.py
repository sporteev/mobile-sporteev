from django.apps import AppConfig


class SportBuddiesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'sport_buddies'
