from django.apps import AppConfig


class SportwikiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'sportwiki'
