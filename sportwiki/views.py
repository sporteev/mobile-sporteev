from django.http import request
from django.shortcuts import redirect, render, resolve_url
from django.views.decorators.csrf import csrf_exempt

def home_sportwiki(request):
    return render(request, 'sportwiki/list-jenis-olahraga.html')


def wiki_details(request):
    return render(request, 'sportwiki/wiki-details.html')
