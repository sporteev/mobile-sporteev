from django.urls import path

from . import views

app_name = 'sportwiki'

urlpatterns = [
    path("", views.home_sportwiki, name="home-sportwiki"),
    path("wiki-details/", views.wiki_details, name="wiki-details"),
]