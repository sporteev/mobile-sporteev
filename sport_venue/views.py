from email import message
from django.http import request, HttpResponse
from django.shortcuts import redirect, render, resolve_url
from django.views.decorators.csrf import csrf_exempt
from pyexcel_xls import get_data as xls_get
from pyexcel_xlsx import get_data as xlsx_get
from django.utils.datastructures import MultiValueDictKeyError
from .models import Venue
from import_export import resources
from .resources import VenueResource
from django.contrib import messages
from tablib import Dataset

def venue_filter(request):
    return render(request, 'sport_venue/venue-filter.html')

def list_venue(request):
    list_venue = list(Venue.objects.all())
    context = {
        'list_venue':list_venue
    }
    return render(request, 'sport_venue/list-venue.html', context)

def venue_details(request, id_venue):
    tmp_venue = Venue.objects.filter(id=id_venue)
    venue = tmp_venue[0]
    nama_venue = venue.nama_venue
    provinsi = venue.provinsi
    kabupaten_kota = venue.kabupaten_kota
    kecamatan = venue.kecamatan
    alamat = venue.alamat
    no_telepon = venue.no_telepon
    harga = venue.harga
    jenis_olahraga = venue.jenis_olahraga
    jadwal = venue.jadwal
    kontak_coach = venue.kontak_coach
    fasilitas = venue.fasilitas
    sewa_alat = venue.sewa_alat
    link_foto = venue.link_foto
    keterangan_tambahan = venue.keterangan_tambahan
    
    context = {
        'nama_venue':nama_venue,
        'provinsi':provinsi,
        'kabupaten_kota':kabupaten_kota,
        'kecamatan':kecamatan,
        'alamat':alamat,
        'no_telepon':no_telepon,
        'harga':harga,
        'jenis_olahraga':jenis_olahraga,
        'jadwal':jadwal,
        'kontak_coach':kontak_coach,
        'fasilitas':fasilitas,
        'sewa_alat':sewa_alat,
        'link_foto':link_foto,
        'keterangan_tambahan':keterangan_tambahan
    }
    return render(request, 'sport_venue/venue-details.html', context)

def upload_data_venue(request):
    if request.method == 'POST':
        venue_resource = VenueResource()
        dataset = Dataset()
        new_venue = request.FILES['file_db']

        # if not (new_venue.name.endswith('xlsx') or new_venue.name.endswith('xls')):
        if not new_venue.name.endswith('xlsx'):
            messages.info(request, 'wrong format')
            return render(request, 'sport_venue/upload.html')
        
        Venue.objects.all().delete()
        imported_data = dataset.load(new_venue.read(), format='xlsx')
        for data in imported_data:
            #cek apakah masih ada data selanjutnya atau engga
            if data[1] == None:
                break
            venue = Venue(
                data[0],
                data[1],
                data[2],
                data[3],
                data[4],
                data[5],
                data[6],
                data[7],
                data[8],
                data[9],
                data[10],
                data[11],
                data[12],
                data[13],
                data[14],
                )
            venue.save()
    return render(request, 'sport_venue/upload.html')
        
