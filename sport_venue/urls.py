from django.urls import path

from . import views

app_name = 'sport_venue'

urlpatterns = [
    path("", views.venue_filter, name="venue-filter"),
    path("list-venue/", views.list_venue, name="list-venue"),
    path("venue-details/<int:id_venue>", views.venue_details, name="venue-details"),
    path("upload-data-venue/", views.upload_data_venue, name="upload-data-venue"),
]