from pyexpat import model
from typing import Callable
from django.db import models
from django.db.models.deletion import CASCADE
from main.models import Pengguna
from django_dropbox_storage.storage import DropboxStorage
# from location_field.models.plain import PlainLocationField
from django.db.models.fields.related import ManyToManyField

# Create your models here.

class Venue(models.Model):
    nama_venue = models.CharField(max_length=50, null=False)
    provinsi = models.CharField(max_length=50, null=False)
    kabupaten_kota = models.CharField(max_length=50, null=False)
    kecamatan = models.CharField(max_length=50, null=False)
    alamat = models.CharField(max_length=100, null=False)
    no_telepon = models.CharField(max_length=50, null=False)
    harga = models.CharField(max_length=200, null=False)
    jenis_olahraga = models.CharField(max_length=100, null=False)
    jadwal = models.CharField(max_length=200, null=False)
    kontak_coach = models.CharField(max_length=50, null=False)
    fasilitas = models.CharField(max_length=200, null=False)
    sewa_alat = models.CharField(max_length=200, null=False)
    link_foto= models.CharField(max_length=200, null=False)
    keterangan_tambahan = models.CharField(max_length=200, null=False)

class Lapangan(models.Model):
    venue = models.ForeignKey(Venue, on_delete=CASCADE, blank=False)
    nama_lapangan = models.CharField(max_length=50, null=False)
