from import_export import resources
from .models import Venue

class VenueResource(resources.ModelResource):
    class meta:
        model = Venue