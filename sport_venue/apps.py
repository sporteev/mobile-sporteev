from django.apps import AppConfig


class SportVenueConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'sport_venue'
