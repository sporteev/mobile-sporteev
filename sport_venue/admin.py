from django.contrib import admin
from import_export.admin import ImportExportModelAdmin
from .models import Venue

# Register your models here.

@admin.register(Venue)
class VenueAdmin(ImportExportModelAdmin):
    list_display = ('nama_venue', 'provinsi', 'kabupaten_kota', 'kecamatan', 'alamat', 'no_telepon', 'harga', 'jenis_olahraga', 'jadwal', 'kontak_coach', 'fasilitas', 'sewa_alat', 'link_foto', 'keterangan_tambahan')