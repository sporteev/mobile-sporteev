from django.http import request
from django.shortcuts import redirect, render, resolve_url
from main.models import Pengguna
from django.views.decorators.csrf import csrf_exempt
from sportivity.models import Event, EventRequest

# Create your views here.

def home_admin(request):
    return render(request, 'admin_dashboard/home-admin.html')