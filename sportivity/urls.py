from django.urls import path

from . import views

app_name = 'sportivity'

urlpatterns = [
    path("", views.home_sportivity, name="home-sportivity"),
    path("buat-event-jenis/", views.buat_event_jenis, name="buat-event-jenis"),
    path("buat-event-general/", views.buat_event_general, name="buat-event-general"),
    path("buat-event-details/", views.buat_event_details, name="buat-event-details"),
    path("buat-event-checking/", views.buat_event_checking, name="buat-event-checking"),
    path("delete-event/", views.delete_event, name="delete-event"),
    path("detail-event/<int:id_event>/", views.detail_event, name="detail-event"),
    path("end-event/<int:id_event>/", views.end_event, name="end-event"),
    path("request-join/<int:id_event>/", views.request_join, name="request-join"),
    path("list-request/<int:id_event>/", views.list_request, name="list-request"),
    path("terima-request/<int:id_event_request>/", views.terima_request, name="terima-request"),
    path("tolak-request/<int:id_event_request>/", views.tolak_request, name="tolak-request"),
    path("log-aktivitas/", views.log_aktivitas, name="log-aktivitas"),
]