from django.http import request
from django.shortcuts import redirect, render, resolve_url
from main.models import Pengguna
from django.views.decorators.csrf import csrf_exempt
from sportivity.models import Event, EventRequest
from profil.views import update_level
import string
import random

# Create your views here.

alpha_numeric = string.ascii_uppercase + string.digits
#generate random string
def id_generator(size=6, chars=alpha_numeric):
    return ''.join(random.choice(chars) for _ in range(size))

def home_sportivity(request):
    list_event = list(Event.objects.filter(status=1))
    context = {
        'list_event' : list_event
    }
    print(list_event)
    return render(request, 'sportivity/list-event.html', context)


def buat_event_jenis(request):
    if request.method == "POST":
        pengguna = Pengguna.objects.get(user=request.user)
        jenis_olahraga = request.POST["jenis_olahraga"]
        event = Event.objects.create(
            jenis_olahraga = jenis_olahraga,
            host = pengguna
        )
        return redirect("/sportivity/buat-event-general/")

    return render(request, 'sportivity/buat-event-jenis.html')

def buat_event_general(request):
    if request.method == "POST":
        pengguna = Pengguna.objects.get(user=request.user)
        list_event = list(Event.objects.filter(host=pengguna))
        event = list_event[-1]
        event.nama_event = request.POST["nama_event"]
        event.deskripsi = request.POST["deskripsi"]
        event.tanggal = request.POST["tanggal"]
        event.waktu_start = request.POST["waktu_start"]
        event.waktu_end = request.POST["waktu_end"]
        event.venue = request.POST["venue"]
        event.alamat = request.POST["alamat"]
        # if request.FILES.get('myfile',False):
        #         new_foto = request.FILES['myfile']

        #         foto_format = "." + ((new_foto.name).split("."))[-1]
        #         new_foto_name = (new_foto.name).replace(foto_format,"")
        #         if pengguna.photo.name != "":
        #             prev_foto_name = ((pengguna.photo.name).split(".")[0])[15:]
        #         else:
        #             prev_foto_name = alpha_numeric
        #         # (petunjuk) new foto name <-- rand(new_foto) + rand(prev_foto) + rand(string) + file formats
        #         new_foto.name = id_generator(6,new_foto_name) + id_generator(6,prev_foto_name) + id_generator() + foto_format

        #         event.photo = new_foto
        event.save()
        return redirect("/sportivity/buat-event-details/")

    return render(request, 'sportivity/buat-event-general.html')


def buat_event_details(request):
    if request.method == "POST":
        pengguna = Pengguna.objects.get(user=request.user)
        list_event = list(Event.objects.filter(host=pengguna))
        event = list_event[-1]
        event.min_participant = request.POST["min_participant"]
        event.max_participant = request.POST["max_participant"]
        event.harga = request.POST["harga"]
        event.level = request.POST["level"]
        # status_is_ranked = request.POST["is_ranked"]
        # status_is_private = request.POST["is_private"]
        # if status_is_ranked == "on":
        #      event.is_ranked = True
        # if status_is_private == "on":
        #      event.is_private = True
        event.save()
        return redirect("/sportivity/buat-event-checking/")

    return render(request, 'sportivity/buat-event-details.html')


def buat_event_checking(request):
    if request.method == "POST":
        pengguna = Pengguna.objects.get(user=request.user)
        list_event = list(Event.objects.filter(host=pengguna))
        event = list_event[-1]
        event.status = 1
        print(event.status)
        update_level(request)
        event.save()
        return redirect("/home")
    
    pengguna = Pengguna.objects.get(user=request.user)
    list_event = list(Event.objects.filter(host=pengguna))
    event = list_event[-1]
    nama_event = event.nama_event
    host = event.host
    deskripsi = event.deskripsi
    tanggal = event.tanggal
    waktu_start = event.waktu_start
    waktu_end = event.waktu_end
    venue = event.venue
    alamat = event.alamat
    min_participant = event.min_participant
    max_participant = event.max_participant
    harga = event.harga
    level = event.level

    context = {
        'nama_event':nama_event,
        'host':host,
        'deskripsi':deskripsi,
        'tanggal':tanggal,
        'waktu_start':waktu_start,
        'waktu_end':waktu_end,
        'venue':venue,
        'alamat':alamat,
        'min_participant':min_participant,
        'max_participant':max_participant,
        'harga':harga,
        'level':level
    }
    return render(request, 'sportivity/buat-event-checking.html', context)

def delete_event(request):
    pengguna = Pengguna.objects.get(user=request.user)
    list_event = list(Event.objects.filter(host=pengguna))
    event = list_event[-1]
    event.delete()
    return redirect("/sportivity/buat-event-jenis/")



def detail_event(request, id_event):
    pengguna = Pengguna.objects.get(user=request.user)
    tmp_event = Event.objects.filter(id=id_event)
    event = tmp_event[0]
    print(event)
    id_event = event.id
    nama_event = event.nama_event
    host = event.host
    deskripsi = event.deskripsi
    tanggal = event.tanggal
    waktu_start = event.waktu_start
    waktu_end = event.waktu_end
    venue = event.venue
    alamat = event.alamat
    min_participant = event.min_participant
    max_participant = event.max_participant
    harga = event.harga
    level = event.level
    jenis_olahraga = event.jenis_olahraga

    context = {
        'id_event' : id_event,
        'nama_event':nama_event,
        'host':host,
        'deskripsi':deskripsi,
        'tanggal':tanggal,
        'waktu_start':waktu_start,
        'waktu_end':waktu_end,
        'venue':venue,
        'alamat':alamat,
        'min_participant':min_participant,
        'max_participant':max_participant,
        'harga':harga,
        'level':level,
        'pengguna':pengguna,
        'jenis_olahraga':jenis_olahraga,
    }
    return render(request, 'sportivity/detail-event.html', context)


def request_join(request, id_event):
    if request.method == "POST":
        pengguna = Pengguna.objects.get(user=request.user)
        event_id = Event.objects.get(id=id_event)
    
        event_request = EventRequest.objects.create(
                event_id = event_id,
                participant = pengguna,
                status_request = 1
        )
        return redirect("/home/")

def list_request(request, id_event):
    list_event_request = list(EventRequest.objects.filter(event_id=id_event))
    context = {
        'list_event_request' : list_event_request,
        'id_event' : id_event
    }
    return render(request, 'sportivity/list-request.html', context)

def terima_request(request, id_event_request):
    if request.method == "POST":
        id_event = request.POST["id_event"]
        event_request = EventRequest.objects.get(id=id_event_request)
        event = Event.objects.get(id=event_request.event_id.id)
        event.current_participant += 1
        event_request.status_request = 2
        event.save()
        event_request.save()
        return redirect("/sportivity/list-request/"+id_event)

def tolak_request(request, id_event_request):
    if request.method == "POST":
        id_event = request.POST["id_event"]
        event_request = EventRequest.objects.get(id=id_event_request)
        event_request.status_request = 3
        event_request.save()
        return redirect("/sportivity/list-request/"+id_event)

def end_event(request, id_event):
    if request.method == "POST":
        event = Event.objects.get(id=id_event)
        event.status = 2                        # set status jadi selesai
        list_event_request = list(EventRequest.objects.filter(event_id=event))
        for event_request in list_event_request :
            event_request.status = 4            # set status jadi selesai
            peserta = event_request.participant
            peserta.total_exp += 10
            peserta.save()
            event_request.save()
        event.save()
        return redirect("/sportivity/")

def log_aktivitas(request):
    if request.method == "GET":
        pengguna = Pengguna.objects.get(user=request.user)
        list_event_host = list(Event.objects.filter(host=pengguna))
        list_event_request = list(EventRequest.objects.filter(participant=pengguna))
        list_event = []
        for event_request in list_event_request :
            list_event.append(event_request.event_id)

        for event_host in list_event_host :
            list_event.append(event_host)

        context = {
        'list_event' : list_event
        }
        print(list_event)
        
        return render(request, 'sportivity/log-aktivitas.html', context)
