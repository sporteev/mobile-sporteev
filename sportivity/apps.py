from django.apps import AppConfig


class SportivityConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'sportivity'
