# Generated by Django 3.2.9 on 2022-01-26 05:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sportivity', '0004_alter_event_level'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='status',
            field=models.IntegerField(choices=[(0, 'Draft'), (1, 'Sukses'), (2, 'Selesai')], default=0, null=True),
        ),
        migrations.AlterField(
            model_name='eventrequest',
            name='status_request',
            field=models.IntegerField(choices=[(0, 'Draft'), (1, 'Menunggu Konfirmasi'), (2, 'Diterima'), (3, 'Ditolak'), (4, 'Selesai')], default=0),
        ),
    ]
