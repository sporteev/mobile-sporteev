from django.shortcuts import redirect, render
# from order.models import Sportivity
from main.models import Pengguna
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required(login_url="/")
def landing_page(request):
    current_user = request.user
    # if current_user.is_staff:
    #     return redirect("/admin-dashboard")
    print(current_user.email)
    return render(request, "landing_page/landing-page.html", {'current_user' : current_user})